const gulp = require('gulp');
const sass = require('gulp-sass');
const babel = require('gulp-babel');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');

//compile scss into css
function style() {
  return gulp.src('app/sass/**/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/dist/css'))
    .pipe(browserSync.stream());
}

exports.style = style;

//compile modern js into polyfilled js
function scripts() {
  return gulp.src('app/js/**/*.js')
    .pipe(babel({
          presets: ['@babel/env']
     }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app//dist/js'))
    .pipe(browserSync.stream());
}

exports.scripts = scripts;

function watch() {
    browserSync.init({
        server: {
           baseDir: "./app",
           index: "/index.html"
        }
    });
    gulp.watch('app/sass/**/*.scss', style);
    gulp.watch('app/js/**/*.js', scripts);
}
exports.watch = watch;
