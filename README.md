# Tully's practice project
This is a project for practicing programming languages, version control (GIT) and everything related to it.

## Installation
First time setup:

Install node: https://nodejs.org/en/
After installation check if you have version 12 or higher installed: $ node -v

Install GULP globally: $ npm install --global gulp-cli

Go to your project root folder: $ cd /yourpath/tully/
Run: $ npm install

Every time a front-end package is updated or installed
Run: $ npm install

## Usage
To use SASS with browserSync:
Go to your project root folder: $ cd /yourpath/tully/
Run: $ gulp watch
